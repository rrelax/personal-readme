
## Tom McAtee's README

Hi! My name is Tom and I am a Support Engineer in the APAC region. I started at GitLab in February of 2022, and I've loved every day here so far!

The purpose of this README is to provide some further context on who I am and how I work. Please feel free to create a merge request if you think this page could be improved! So far in my [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) journey i've met people from the [following countries (updated 28th of November, 2022)](https://www.amcharts.com/visited_countries/#FR,DE,NL,RO,RU,UA,CA,MX,US,EG,KE,ZA,KH,IN,ID,JP,MY,PH,SG,KR,AU,NZ).

I've separated my readme into some further files, here's the index:

1. Personal
   - [Media I like](/personal/media.md)
   - [Tools I Like](/personal/tools-I-like.md)
2. [Projects](/projects/index.md)
3. Work
   - [Office setup](/work/office-setup.md)

### Working with me!
#### Strengths

- I have a **_lot_** of ideas, and i'm eternally curious (Please, tell me about anything you find interesting!)
- I'm blessed to know and love people from a diverse variety of backgrounds and personal outlooks, so I'd like to think I'm very inclusive.
  - If I've done or said something that isn't very inclusive, please let me know - I like to, so I appreciate the feedback

#### Weaknesses

- I have a **_lot_** of ideas, and i'm eternally curious - which can mean I can get distracted. If i'm getting sidetracked or ahead of myself, please point this out (or tell me to take a note of it and move on) - I won't be upset!
- My reply times can be a bit whack - if I'm taking too long, feel free to send a follow-up message.
- I've been working on it, but I still have a tendancy to over-commit and over-extend. If we're working together on a project, a useful question for me is: "_what can we delegate around the team?_"


#### Feedback

- Please send me any and all feedback! Nobody knows everything, and it's a rare process that can't be optimised.
- I'm happy to receive feedback through pretty much any one-on-one channel (whether that's internal GitLab comms or external).

