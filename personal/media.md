
### Reading/Watching List

I'm a voracious consumer of news media, from a wide spectrum of sources - [this page](https://api.mcatee.io/pocket/) is a good representation of the articles I've been reading (keep in mind the tags link to index pages!).

#### Books

##### Non-Fiction
<details>
<summary>Click to show my non-fiction book reccomendations!</summary>
- **Atomic Habits** by James Clear
  I'm not usually somebody who enjoys self-help books, but this is _incredible_. If you feel like you get demoralised easy when trying to start a new habit (or if you're not sure where to begin!), this is the book for you.

- **[Debt: The First 5000 Years](https://en.wikipedia.org/wiki/Debt:_The_First_5000_Years)** by David Graeber (RIP)
  A book that probes some intuitions-at-large regarding what came before our current economic system. With a fine anthropological microscope Graeber examines the evidence for _barter systems_, and instead finds evidence of a diverse set of historical economic systems such as [gift economies](https://en.wikipedia.org/wiki/Gift_economy) and credit debt that existed long before currency did in the form we know it.

- **Living With Complexity** by Donald Norman

  What's does it mean for something to be `complex` versus being `complicated`? When you struggle to make sense of a new tool or appliance, are you really at fault - or was it designed poorly, or potentially for somebody with a particular set of existing knowledge? _Living With Complexity_ contains a great set of terms and tools for analysing interfaces and design in general - Norman has released quite a number of books in this "_series_", however I view each one as a refinement over the last (And it has some beautiful meta moments, such as that the first book _The Psychology of Everyday Things_ was renamed _The Design of Everyday Things_ as it kept being stocked in the **Psychology** section due to it's name - a slight example of poor design in action!)

</details>

##### Fiction
<details>
<summary>Click to show my fiction book reccomendations!</summary>
- Works by **Kurt Vonnegut**

  Vonnegut is my favourite author of all time; his narrative voice feels comfortable like sinking into a warm bath, and his phrasing is incredible simple, yet conveys complex meanings. I have trouble recommending a particular book of his, so here's some short summaries of my favourites:

<p>
<details>
<summary>Click to show my Kurt Vonnegut summaries.</summary>

  - **Cat's Cradle** (1963)
    The _ultimate_ cold war science fiction book - what if a mad scientist created a new form of ice that froze all water it touched? How could that go wrong? Cross this with a weird contradictory cult and an odd family, and you've got my top Vonnegut pick.

  - **Slaughterhouse 5** (1969) (Also known as _The Children's Crusade: A Duty-Dance with Death_)
    A _semi-biographical science fiction story_ - Vonnegut served in the US army during World War 2, and was taken as a prisoner of war in the city of Dresden. Vonnegut survived the [firebombing of Dresden](https://en.wikipedia.org/wiki/Bombing_of_Dresden_in_World_War_II) as he and the other prisoners were living in a massive, concrete decommisioned Slaughterhouse which shielded them from certain-death. This book is an explanation of his experiences, although crossed with a science-fiction story about Billy, a dork who becomes unstuck in time due to alien influecen, and experiences all moments of his life both out of order and at the same time.

  - **Mother Night** (1961)

    _"This is the only story of mine whose moral I know. I don't think it's a marvelous moral, I simply happen to know what it is: We are what we pretend to be, so we must be careful about what we pretend to be."_

    _Mother Night_ is about a fictious American double-agent Howard W. Campbell Jr., who was to act as a Nazi radio propagandist but secretly pass back information to the Allies. The book begins with Campbell talking to the reader from a Mossad jailcell, awaiting his execution. The foreword quote above is an example of Vonnegut's simple, elegant writing style.

  - **Timequake** (1997) 

    _What if there was an earthquake, but in time? And the effect was that **everyone** has to repeat the past year of their lives (every blink and every stubbed toe), while being both consciously aware of the repeat, but also on complete autonomous 'auto-pilot'_

</details>
</p>

- **After The Siege** by Cory Doctorow

    _After The Siege_ is similar to _Slaughterhouse 5_, in that it's a transformative work of science fiction that attempts to relays Doctorow's grandmother's experiences of the Siege of Leningrad during World War 2 (but also with zombies and intellectual property disputes). I'd recommend _all_ of Doctorow's work; he's a prolific author who has written _lots_ of short stories and about most tech things (_Information Does Not Want To Be Free_ is a great read on DRM).

- **Battle Royale** by Koushun Takami

  The book that inspired _Fortnite_, _PUBG: Battlegrounds_, and every other Battle Royale mode. The short summary is: in this alternative timeline, Japan has an fascistic government that picks a sample of high school classses across the country, takes each class to an island, and gives them one directive: "_Only one student can leave the island, otherwise **no** students will leave the island_".

- **[The Murderbot Diaries](https://en.wikipedia.org/wiki/The_Murderbot_Diaries)** by Martha Wells

  _What if there was a guard robot AI that hacked itself to gain sentience, and so spends all it's spare time (when it's not pretending to not be sentient) watching Netflix?_

- Short stories by **Nancy Kress**

  Nancy Kress has a beautiful (several Hugo award winning) writing style - it reminds me of Vonnegut's simplicity, but potentially with better pacing. She's quite prolific, but I think _[After the Fall, Before the Fall, During the Fall](https://www.goodreads.com/book/show/13163688-after-the-fall-before-the-fall-during-the-fall)_ is a good starting point. Additionally Kress has written [an amazing book about writing](https://www.goodreads.com/book/show/68317.Beginnings_Middles_Ends) and continues to write full-length novels such as _[Beggars in Spain](https://www.goodreads.com/book/show/68333.Beggars_in_Spain)_, the start of a series that speculates on how society would change if a section of the population didn't need to sleep anymore).


- Works by **Neal Stephenson**

  The below two books are two sides of the same coin to me - reading one feels like a critique of the other. **Snow Crash** is a dystopian speculation on "_what if anarcho-capitalism actually became a thing_", while **The Diamond Age** approaches the question of "_what would a post-scarcity economic situation look like if we were still as (or got more) tribalistic than we are currently?_".
<p>
<details>
<summary>Click to show my Neal Stephenson summaries.</summary>

  Part of Stephenson's narrative style that really draws me in is his way of synthesising a _lot_ of different domains into his stories - for example **Snow Crash** discusses memes (cyber and social) and information contagions, sumerian theology, virtual reality, the Tower of Babel as a metaphor for an infocalyptic event (_this metaphor is generally only a few steps away from my conscious mind at any point_), and even the end-result of massive privatisations on state sovereignty. 
  
  To demonstrate the spectrum of the domains in Stephenson's books - none of the above themes appear in **The Diamond Age** - instead it discusses confucianism, revolution, post scarcity economics (via the device of _matter compilation_, nano-scale 3D printing as a utility in every home), the future of digital (and adaptive) pedagogy, and social class.

  - **The Diamond Age**
  
  _What if when you were an infant, you were given a book - a book with a simple story that teaches you a somewhat simple concept, like say... addition. What if, the next time you read the book, the story has gotten more complex and this time is teaching you about some social skills? What if, a few years down the track the book starts teaching you **subversion**? What would happen to you? What would happen to society if this sort of device became common-place?_

  - **Snow Crash**

  _What if there was a mental virus, transmissible via virtual reality? What if the metaverse really existed?_

</details>
</p>

- **Horror**
  I _really_ like horror books when they're done well and can make me uncomfortable (most of horror is so cheesy it makes me laugh)

<p>
<details>
<summary>Click to show my horror book reccomendations!</summary>
  - **There Is No Antimemetics Division** by qntm
    This text started off as a series of pages on the [SCP Foundation wiki](https://scp-wiki.wikidot.com/antimemetics-division-hub) (a collaborative horror story wiki), but qntm consolidated the anthology/series into [a single book](https://www.amazon.com.au/There-No-Antimemetics-Division-qntm-ebook/dp/B08FHHQRM2) which I regard as some top-tier memory horror.

    _What if there were monsters that could erase themselves from your thoughts?_

  - **[Dark Water](https://en.wikipedia.org/wiki/Dark_Water_(short_story_collection))** by Koji Suzuki

    A collection of uncomfortable Japanese horror stories themed around water - I really appreciate these stories for just how comfortable they are (especially the cave diving one) and credit them for getting me into literary horror.

  - **[John Dies at the End](https://en.wikipedia.org/wiki/John_Dies_at_the_End)** by David Wong (pen-name of [Jason Pargin](https://en.wikipedia.org/wiki/Jason_Pargin), former Cracked head editor)  .
    It's crude, imaginative, and a lot of fun to read - John and Dave are supernatural investigators, investigating anything that would get you laughed out of a usually investigator's office. Think your car is haunted? Keep seeing a figure composed entirely of a swarm of flies in your local diner? See a weird spectre in the mirror everytime you brush your teeth? Call John and Dave! 
    
    _What if we could only see one layer of reality? What if there was a drug, that open your eyes to the other, horrifying layers, but also made you visible to the denizens of these layers? What if as a side effect, the drug made you omnipotent for a couple of minutes?_

  - Works by **Cassandraw Khaw**

    Cassandra Khaw writes _amazing_ cosmic horror short stories - their works often feel inspired by Lovecraft but merge in aspects of some  other genres such as noir in [Hammers on Bone](https://www.goodreads.com/book/show/30199328-hammers-on-bone).

</details>
</p>
</details>


#### Television/Stream Series
<details>
<summary>Click to show my TV/Stream series reccomendations!</summary>
- **Arrested Development**

- **The Venture Bros**

- **Arrested Development**



</details>
