
# Tools I Like

Here are some tools I use frequently

## Photopea

A reasonably [feature complete HTML5 clone of Adobe Photoshop CS2](https://photopea.com)

## Freeplane

FOSS mind mapping software that helps me keep track of workflows and triaging tickets!

## Pocket

I use Pocket as my bookmarking/RSS solution - you can see [my reading list here](https://api.mcatee.io/pocket).

I have a Zapier and AWS Lambda set up to pull from RSS feeds and add new RSS entries to my pocket list :)
