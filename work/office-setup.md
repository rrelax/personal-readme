
#### Home office setup 
| Item | Purpose | Notes |
| --- | --- | --- |
| [L-shaped desk](https://www.eliteofficefurniture.com.au/products/rwcws1212?variant=34636159451285) | | | 
| [Logitech C922 Pro Stream Webcam](https://www.amazon.com.au/Logitech-1080P-Stream-Webcam-C922/dp/B01M35CNS8) | | | <ul><li>Handily, this comes with a mini tripod!</li><li>This does not have a USB-C connector, so requires an adaptor _or_ dock to work with USB-C only MBPs |
| [Macbook Pro](https://www.apple.com/au/shop/buy-mac/macbook-pro/14-inch) | | <ul><li>2021 model</li><li>14 inch form factor</li><li>Apple M1 Max</li><li>32 GB RAM</li></ul> |
| [Magic Trackpad](https://www.apple.com/au/shop/product/MMMP3ZA/A/magic-trackpad-black-multi-touch-surface) | | |
| [Magic Keyboard  with Numeric Keypad](https://www.apple.com/au/shop/product/MQ052ZA/A/magic-keyboard-with-numeric-keypad-us-english) | | |
