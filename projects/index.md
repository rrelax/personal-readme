
### Projects

#### GitLab Related

##### Userscripts
Userscripts! I tend to optimise webpages I use often by creating userscripts to ensure they work _just_ how I'd like them to.

I'll insert links to my scripts later, however you should be able to see them in my public repositories :)
| Summary | Operates On | Notes | Link |
| --- | --- | --- | --- |
| Adds search URLs to Customer pages | Customer Portal | Adds a handy "search this email in licenses" to each Customer page | [x] |
| Add 'search other model' links | Customer Portal | Adds a handy "repeat this query on [licenses\|customers]" on the opposite model's search results page | [x] |


##### Stat Generation Scripts



#### Outside of GitLab

##### Machines 

I collect machines for making things!

| Machine | Brand  | How I Got It | What I Do With It |
| --- | ---| --- | --- |
| Resin Printer | Photon Mini 4K  | Purchased | 3D Printing Miniatures and models! |
| FDM Printer | Creality Ender 4 Pro | Purchased | 3D Printing Bits and pieces for around the house, and paint stencils | 
| Sublimation Printer | | Bought on Facebook Marketplace | I use this to print transfers for making printed mugs and fabric prints |
| Monochrome Laser Printer | Brother LD2713DW | Purchased | I use this for printing text blocks for book binding |   
| Paper Folder | (Ancient, unclear) | Free off of Facebook Marketplace | I use this for folding zines for distribution | 
| Photo Printer | Canon IP8760 | Purchased | Printing stickers and A4/A3 photos for gifts |
| Badge Press | Generic | Purchased | Pressing Badges For Local Community Groups |
| Heat Press | Generic | Purchased | Making sewable patches, applying vinyls to shirts/hoodies, sublimating mugs |
| Vinyl Cutter | Silhouette 4 Plus | Purchased (would not recommend) | Used for cutting vinyls for apparel and kiss-cut stickers |


##### Cameras

I like cameras, especially old and new polaroids!

| Brand | Model | Level of Function | Status | 
| --- | --- | --- | --- | 
| Polaroid | SX-70 | Sadly, non-functional and there are no local repairers | Owned |
| Fujifilm | Mini 6 | Functional, very used and loved | Owned |
| Fujifilm | Evo | - | Unowned, seeking |
| Fujifilm | SQ6 | - | Unowned, seeking |


##### Software & Code

###### Serverless

I love serverless applications - in fact I think IaaS solutions could be "the next big thing" for consumers looking to avoid massive, privacy-antagonistic services like Facebook or Twitter, and could further enable everyday users to run useful services like RSS readers (RIP Google Reader), personal music hosting/streaming services (RIP Google Play Music) or personal web archiving!

Currently I'm working on a serverless implementation of the ActivityPub protocol - it's slow-going since I want to get it right, so I don't have anything to display yet.

Some brief notes though:
- The system utilises serverless microservices deployed using the [Serverless Framework compose function](https://www.serverless.com/framework/docs/guides/compose)
   - Using microservices means that _potentially_ components could be modular, and thus mixed and matched for different applications/platforms (for example you could run your email service on AWS to access SQS, and then your GUI could be served by a microservice on DigitalOcean, and so forth).
- Currently it's written for AWS specifically as I don't have comparable experience with other IaaS vendors

###### Accessibility

I'm reasonably able-bodied and I often take it for granted that the internet is constructed for people of similar ability to me - it makes me uncomfortable that sometimes the tools or software I make may not be as accessible for other people, so I'm learning how to implement different accessibility methods for web interfaces.

Something that's particularly interesting to me is potentially creating secondary interfaces for specific accessibility use cases rather than bolting on something as an afterthought. For example, using ARIA tags on a chat application (with many incoming events such as new messages, reactions, threads, not to mention channel events!) seems like a janky solution, versus something specifically developed for screen reader applications.

If you have an interest or experience in web accessibility I'd love to hear about it!


###### OSINT

Open Source Intelligence (or OSINT) is a fascinating domain to me - there's so much information on the internet (and accessible through the internet), I find it interesting in how it can be tied together.

Projects I watch in this space:

| Project Name | Purpose | Why I'm Interested | Notes |
| --- | --- | --- | --- |
| [Ushahidi](https://www.ushahidi.com/) | | | |
| [LiveUAMap](https://www.liveuamap.com)| | | |
| [Bellingcat](https://bellingcat.com) | | | | 